<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
    <?php include('contenido/head.php'); ?>
</head>

<body><em></em>

	<?php include('chat.php'); ?>

	<div id="container">
		 <?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>
    
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>La Página que buscas no existe, Te dejamos nuestros datos de contacto.</h2>
					

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">

                        	<div class="container">
								<div class="col-md-12" >
									<a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">
										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">
										</i> <font size="6"> WhatsApp</font> 
									</button>
								</a>
							</div>
						</div>

						<br>

                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('form.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li>
										<span><i class="fa fa-home"></i>Libramiento Oriente No. 5675, </span> 
										<span> Col. Quirindavara,  </span> 
										<span> Uruapan, Michoacán</span>
										<span> C.P. 60190</span> 
									</li>
									<li><span><i class="fa fa-phone"></i>01 (452) 528 8815</span></li>
									

									<!--

									<li><i class="fa fa-phone"></i><span>Recepción <strong> </strong></span><br>                                    <i class="fa fa-phone"></i><span>Taller de Servicio <strong> </strong></span><br>
                                   	<i class="fa fa-phone"></i><span>Refacciones <strong> </strong></span><br>
                                    <i class="fa fa-phone"></i><span>Postventa <strong> </strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong> </strong></span><br>
                                    -->      
                                    
                                    </li>
                                    
                               <p>
                                <h3>WhatsApp</h3>

                               <li><span><i class="fa fa-whatsapp"></i><strong> Ventas <br> 

                               	<a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola,%20Quiero%20más%20información!" title="Ventas Nuevos">
												442 592 38 62
											</a>


                                </strong></span></li>

                               
<!-- 
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Servicio <br>  xxxxxxxxxx </strong></span></li>

                               
                              </p>
                                    
									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@famevalladolid.com</a></li>
								</ul>

-->


							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Toyota FAME Uruapan</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p><br>
                                
                                <center>
                                <h3>TOYOTA Uruapan</h3>
                                <P class="work-time">Libramiento Oriente No. 5675, </P>
                                <P class="work-time">Col. Quirindavara, Uruapan, Mich. </P>
                                <p class="work-time">C.P. 60190</p>
                                </center>
							</div>
						</div>


				</div>
		</div><br>
        </div>
		
			<?php include('contenido/footer.php'); ?>
     </div> 			
		
</body>
</html>