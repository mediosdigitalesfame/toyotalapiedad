<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Autos</title>
	<?php include('contenido/head.php'); ?> 
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>
	 <div id="content">
		 <div class="page-banner">
			 <div class="container">
				 <h2>Vehículos</h2>
			 </div>
		 </div>

		 <div class="portfolio-box with-4-col">
			 <div class="container">
				 <ul class="filter">
					 <li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>
					 <li><a href="#" data-filter=".auto">Autos</a></li>
					 <li><a href="#" data-filter=".suv">SUV's - Minivan</a></li>
					 <!--<li><a href="#" data-filter=".camioneta">Camioneta</a></li>
					 <li><a href="#" data-filter=".hibrido">Híbridos</a></li>--> 
				 </ul>

				 <div class="portfolio-container">

						 <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/originales/chr2019.png">
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CH-R <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								 <span><a href="pdfs/chr2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
				         </div> 

				         <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/originales/yarisr2018.jpg">
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> YARIS R <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								 <span><a href="pdfs/yaris-r.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
				         </div> 
 
						<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="Toyota® Yaris Sedán 2018" src="images/autos/originales/yarissedan2019.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> YARIS <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/yarissedan2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>

				         <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/originales/camry2019.jpg">
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CAMRY <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								 <span><a href="pdfs/camry2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
				         </div> 

				         <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/originales/camryh2019.png">
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CAMRY HV<sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								 <span><a href="pdfs/camryh2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
				         </div> 

 
						<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="Toyota® Avanza 2019" src="images/autos/originales/avanza2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> AVANZA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/avanza2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
 
						<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="Toyota® Corolla 2019" src="images/autos/originales/corolla2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> COROLLA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/corolla2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div> 
                                 
						<div class="work-post comercial">
							<div class="work-post-gal">
								<img alt="Toyota® Hiace 2019" src="images/autos/originales/hiace2019.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> HIACE <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/hiace2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>                        
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="Toyota® Highlander 2018" src="images/autos/originales/highlander2019.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> HIGHLANDER <sup>&reg;</sup> <font color="#DD1707">19</font> </em> </strong></h2>
								<span><a href="pdfs/highlander2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>

						<div class="work-post hibrido">
							<div class="work-post-gal">
								<img alt="Toyota® Prius C 2019" src="images/autos/originales/priusc2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> PRIUS C<sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/prius-c2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>  
                   
						<div class="work-post hibrido">
							<div class="work-post-gal">
								<img alt="Toyota® Prius 2018" src="images/autos/originales/prius2018.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> PRIUS <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/prius.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="Toyota® RAV 4 2018" src="images/autos/originales/rav42018.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> RAV 4 <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/rav4.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
							<img alt="Toyota® Sequoia 2018" src="images/autos/originales/sequoia2018.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> SEQUOIA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/sequoia.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="Toyota® Sienna 2019" src="images/autos/originales/sienna2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> SIENNA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/sienna2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>                            
                            
						<div class="work-post comercial">
							<div class="work-post-gal">
								<img alt="Nueva Tacoma 2019" src="images/autos/originales/tacoma2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> TACOMA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/tacoma2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>						
                        

						<div class="work-post comercial">
							<div class="work-post-gal">
								<img alt="Toyota® Tundra 2019" src="images/autos/originales/tundra2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> TUNDRA <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/tundra2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>

						 <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/originales/hiluxdiesel4x42019.png">
							 </div>
							 <div class="work-post-content">
                                 <h2> <strong> <em> HILUX D  4X4 <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								 <span><a href="pdfs/hiluxdiesel2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
				         </div> 

				         <div class="work-post auto">
							<div class="work-post-gal">
								<img alt="Toyota® Yaris Hatchback 2018" src="images/autos/yarishb2018.jpg">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> YARIS HB <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/yaris-hb.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
 
						<div class="work-post comercial">
							<div class="work-post-gal">
								<img alt="Nueva Hilux 2019" src="images/autos/originales/hilux2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> HILUX <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/hilux2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>  

						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="Toyota® Land Cruiser 2019" src="images/autos/originales/landcrusier2019.png">
							</div>
							<div class="work-post-content">
								<h2> <strong> <em> LAND CRUISER <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2>
								<span><a href="pdfs/landcruiser2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>

					 </div>
	             </div>
		     </div>
         </div>
     </div>
 <!-- FIN VEHICULOS -->
 	
 <?php include('contenido/footer.php'); ?>

 </body>
</html>